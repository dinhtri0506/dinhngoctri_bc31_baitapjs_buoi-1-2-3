function perimeterAndAreaOfRectangleFormula() {
  // Input var
  // Lấy dữ liệu đầu vào.
  // Chiều dài hình chữ nhật.
  var lengthRectangleValue =
    document.getElementById("length-rectangle").value * 1;
  // Chiều rộng hình chữ nhật.
  var breadthRectangleValue =
    document.getElementById("breadth-rectangle").value * 1;
  // Notice var
  var lengthNoticeEl = document.getElementById("helpId-length");
  var breathNoticeEl = document.getElementById("helpId-breadth");
  var noticeIfValueLessThanZero = "phải >= bằng 0";

  // Formula var
  // Các bước xử lý.
  // Lập công thức tính chu vi. Chu vi hình chữ nhật = (Chiều dài + Chiều rộng) * 2
  var perimeterOfRectangle = (lengthRectangleValue + breadthRectangleValue) * 2;
  // Lập công thức tính diện tích. Diện tích hình chữ nhật = Chiều dài * Chiều rộng
  var areaOfRectangle = lengthRectangleValue * breadthRectangleValue;
  // Kiểm tra xem người dùng có cố tình nhập vào giá trị âm hay không.
  if (lengthRectangleValue < 0 || breadthRectangleValue < 0) {
    // Nếu có, hiện lên đoạn thông báo và không in ra kết quả.
    lengthNoticeEl.innerHTML = noticeIfValueLessThanZero;
    breathNoticeEl.innerHTML = noticeIfValueLessThanZero;
  } else {
    // Nếu không, xoá đi đoạn thông báo và in ra kết quả.
    document.getElementById(
      "perimeter-area-of-rectangle"
    ).innerHTML = `Chu vi: ${perimeterOfRectangle} <br /> Diện tích: ${areaOfRectangle}`;

    // Kết quả đầu ra.
    // Chu vi, diện tích hình chữ nhật.
    console.log("Chu vi hình chữ nhật:", perimeterOfRectangle);
    console.log("Diện tích hình chữ nhật:", areaOfRectangle);
    lengthNoticeEl.innerHTML = "";
    breathNoticeEl.innerHTML = "";
  }
}
