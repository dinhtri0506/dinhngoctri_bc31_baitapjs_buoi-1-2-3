function caculateAverageValue() {
  // Lấy dữ liệu đầu vào.
  // Tạo biến và ép kiểu dữ liệu.
  var firstNum = document.getElementById("first-number").value * 1;
  var secondNum = document.getElementById("second-number").value * 1;
  var thirdNum = document.getElementById("third-number").value * 1;
  var fourthNum = document.getElementById("fourth-number").value * 1;
  var fifthNum = document.getElementById("fifth-number").value * 1;
  var averageValue = 0;

  // Các bước xử lý.
  // Giá trị trung bình của 5 số = (số thứ nhất + số thứ 2 + ... số thứ năm) / 5
  averageValue = (firstNum + secondNum + thirdNum + fourthNum + fifthNum) / 5;

  // Kết quả đầu ra.
  //Giá trị trung bình của 5 số.
  console.log("Giá trị trung bình của 5 số:", averageValue);
  document.getElementById("average-number").value = averageValue;
}
