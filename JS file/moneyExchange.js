function moneyExchange() {
  // Lấy giá trị đầu vào.
  // Số tiền người dùng muốn quy đổi.
  var userInputValue =
    document.getElementById("amount-user-want-to-exchange").value * 1;

  // Các bước xử lý.
  // Tạo biến cho tỷ giá hối đoái.
  var exchangeRate = 23500;
  // Số tiền VND quy đổi được = Số tiền người dùng muốn quy đổi * Tỷ giá hối đoái.
  var amoutHasBeenExchange = 0;
  amoutHasBeenExchange = userInputValue * exchangeRate;

  // Kết quả đầu ra.
  // Số tiền VND quy đổi được (thêm NumberFormat cho user dễ nhìn).
  document.getElementById("amount-has-been-exchange").innerHTML =
    new Intl.NumberFormat("de-DE", {
      style: "currency",
      currency: "VND",
    }).format(amoutHasBeenExchange);
  console.log("Số tiền VND quy đổi được:", amoutHasBeenExchange);
}
