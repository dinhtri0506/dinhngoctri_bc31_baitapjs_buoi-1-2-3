function payroll() {
  // Lấy dữ liệu đầu vào.
  // Tiền lương một ngày.
  var daySalary = document.getElementById("a-day-salary").value;
  // Số ngày làm việc.
  var numberOfWorkingDays = document.getElementById(
    "number-of-working-days"
  ).value;

  // Các bước xử lý.
  // Lương được nhận (khi chưa nhập dữ liệu).
  var salaryReceived = 0;
  // Lương được nhận (kết quả) = Tiền lương 1 ngày * Số ngày làm việc
  salaryReceived = daySalary * numberOfWorkingDays;

  // Kết quả đầu ra.
  // Lương được nhận.
  console.log("Lương được nhận:", salaryReceived);
  document.getElementById("salary-received").value = salaryReceived;
}
