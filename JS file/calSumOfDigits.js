function calSumOfDigits() {
  var numValue = document.getElementById(
    "num-to-calculate-sum-of-digits"
  ).value;
  // Biến hàng chục
  var tensValue = parseInt(numValue / 10);
  // Biến hàng đơn vị
  var unitsValue = parseInt(numValue % 10);
  // Tổng 2 ký số
  var sumOfTwoDigits = tensValue + unitsValue;
  // In ra màn hình tổng 2 ký số
  document.getElementById("sum-of-digits").innerHTML = sumOfTwoDigits;

  // Tính tổng n ký số
  var sumOfDigits = 0;
  // Tạo vòng lặp để tách các ký tự từ chuỗi -> Ép kiểu dữ liệu cho ký tự vừa tách -> Cộng các ký tự lại
  for (i = 0; i < numValue.length; i++) {
    sumOfDigits = sumOfDigits + numValue.charAt(i) * 1;
  }
  console.log("Tổng n ký số:", sumOfDigits);
}
